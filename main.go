package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	args := os.Args[1:]

	if len(args) == 0 {
		fmt.Println("Please provide a file directory")
		return
	}

	files, err := ioutil.ReadDir(args[0])

	if err != nil {
		fmt.Println(err)
		return
	}

	for _, file := range files {
		if file.Size() == 0 {
			fileName := file.Name()
			err := os.Remove(fileName)

			if err != nil {
				fmt.Println("Failed to delete empty file: " + fileName)
				return
			} else {
				fmt.Println("Successfully deleted empty file:  " + fileName)
			}
		}
	}

}
