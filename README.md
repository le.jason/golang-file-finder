# GoLang File Finder

Finds empty files in a specific directory and deletes them.

# Run

go run main.go <directory_name>